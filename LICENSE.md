## License information

The hardware design and user libraries are licensed under [CERN Open Hardware Licence Version 2 - Permissive](/testing_in_a_box_hardware/CERN-OHL-P.txt). 

The case STLs and 3MF files are licensed under [Creative Commons CC-BY-SA 4.0 License](/testing_in_a_box_hardware/CC-BY-SA-4.txt)

This project uses libraries from KiCAD 7 which are licensed under [Creative Commons CC-BY-SA 4.0 License](/testing_in_a_box_hardware/CC-BY-SA-4.txt) with the following exception: 

*To the extent that the creation of electronic designs that use 'Licensed Material' can be considered to be 'Adapted Material', then the copyright holder waives article 3 of the license with respect to these designs and any generated files which use data provided as part of the 'Licensed Material'.*

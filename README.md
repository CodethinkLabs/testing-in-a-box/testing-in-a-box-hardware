# Testing a box hardware

> :warning: **Check Releases For Stable Versions**

## Version 2, Revision B

### Fixed in Version 2, Revision B

* USB hub inputs 2 and 3 do not work as their data lines are connected incorrectly: Fixed #35
* I2C bus SCL and SDA lines connected incorrectly: Fixed #39
* Capacitor C41 connected incorrectly in reverse polarity: Fixed #41
* Missing VBUS discharge circuitry: Fixed #42
* Silkscreen for 'Optocoupler Enable' is incorrect: Fixed #43
* Missing protection diodes on USB-C interface: Fixed #44
* N-Channel MOSFET incorrectly used in bill of materials instead of P-Channel MOSFET: Fixed #45
* Incorrect QSPI_SS and flash_bootstrap circuitry: Fixed #46
* Thermal issues: Fixed #51
* Improve silkscreen markings: Implemented #47
* Potentially improve channel routings: Implemented #48
* Add hardware limitation so only one power output can be selected at a time as a precaution: Implemented #50

## About

Testing in a box hardware is a compact one-stop-shop solution that provides communication with a device under test via CAN, SPI, I2C and serial. It also allows users to connect up to 3 additional USB peripherals and is equipped with USB HID emulation and optocoupled GPIOs.

## Product images

![TIAB hardware images - 1](photos/revb_exploded.png)

![TIAB hardware images - 2](photos/revb_case.png)

## Pin out diagram

![ TIAB pinout diagram ](photos/Version-2_Revision-B_Top_Pinout.png)

### FT4232H

Each of the channels A and B can be setup as either:

* Multi Protocol Synchronous Serial Engine (MPSSE) - JTAG, SPI, I2C, etc.
* UART
* Bit-Bang

Each of the channels C and D can be setup as either:

* UART
* Bit-Bang

The RP2040 SWD connection is connected to channel A, so can only be used when not using channel A for other functionality.

The RP2040 serial connection is connected to channel D, so can only be used when not using channel D for other functionality.

Optocouplers 1, 2, and 3 are connected to ADBUS5, ADBUS6, and ADBUS7 (channel A) respectively, and so can only be controlled when not using the SWD connection, or using channel A for any other functionality.

## Installing KiCAD 7

Hardware is developed using kicad 7. Download instructions can be found [here](https://www.kicad.org/download/).

## Project Setup

Project uses sub-modules to get symbols and footprints. To clone to repo run:

```bash
git clone --recurse-submodules -j4  git@gitlab.com:CodethinkLabs/testing-in-a-box/firmware-hardware/testing-in-a-box-hardware.git
```

## How To Use

**NOTE**: Tools for TIAB hardware are currenly under development.

* Documentation and info about USB switch can be found [here](https://gitlab.com/CodethinkLabs/usb-switch).
* TIAB hardware makes use of the FT4242H which can used for serial connections, programming devices via JTAG and SWD, general purpose I/O, for communications protocols such as I2C and SPI, and more. The datasheet for the FT4232H can be found [here](https://ftdichip.com/wp-content/uploads/2024/09/DS_FT4232H.pdf). Docs for pylibftdi can be found [here](https://pylibftdi.readthedocs.io/en/0.12/how_to.html).
* The onboard RP2040 allows control of the RGB status LED, which could be used for indicating the status of a test. It allows emulation of HIDs (Human Interface Devices) through the HID output port. It also runs the protocol layer for the USB-PD. And more! Documentation for the RP2040s firmware can be found [here](https://gitlab.com/CodethinkLabs/testing-in-a-box/firmware-hardware/tiab-io-board-firmware).

### How To Connect I2C

[pyftdi I2C wiring](https://eblot.github.io/pyftdi/api/i2c.html#wiring)

| PORT ON THE IDC   | I2C PORT |
| ----------------- | -------- |
| GND               | GND      |
| +3.3v             | VCC      |
| Channel A/B 0     | SCL      |
| Channel A/B 1     | SDA      |
| Channel A/B 2     | SDA      |

### How To Connect SPI

[pyftdi SPI wiring](https://eblot.github.io/pyftdi/api/i2c.html#wiring)

| PORT ON THE IDC | SPI PORT |
| --------------- | -------- |
| GND             | GND      |
| +3.3v           | VCC      |
| Channel A/B 0   | SCK      |
| Channel A/B 1   | SDI      |
| Channel A/B 2   | SDO      |
| Channel A/B 3   | CS       |

### How To Connect Serial

[FT4232 Datasheet Section 3.1.2 Pin Descriptions](https://ftdichip.com/wp-content/uploads/2024/09/DS_FT4232H.pdf)

| PORT ON THE IDC   | SERIAL PORT |
| ----------------- | ----------- |
| Channel A/B/C/D 0 | TX          |
| Channel A/B/C/D 1 | RX          |
| Channel A/B/C/D 2 | RTS         |
| Channel A/B/C/D 3 | CTS         |
| Channel A/B/C/D 4 | DTR         |
| Channel A/B/C/D 5 | DSR         |
| Channel A/B/C/D 6 | DCD         |

## USB VID and PID Information

Codethinks USB VID is `0x27BD`

The PID for the USB hub is `0x0D`

The PID for the FT4232H is `0x0E`
